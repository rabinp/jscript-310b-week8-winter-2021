document.addEventListener("DOMContentLoaded", () =>{
let body = document.querySelector("body");
let colorValue = 255;

const colorChangeInterval = setInterval (function () {
    if (colorValue < 200){
        clearInterval(colorChangeInterval);
    }
    colorValue -= 5;
    body.style.backgroundColor = `rgb(${colorValue}, ${colorValue}, ${colorValue})`
}, 400);

});


