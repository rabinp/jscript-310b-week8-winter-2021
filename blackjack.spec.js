// 1. Create unit tests with Jasmine to test the following cases:

// 10, 9 passed in to function should return false
// Ace, 6 passed in to function should return true
// 10, 6, Ace passed in to function should return false
// 2, 4, 2, 5 passed in should return true

describe("Soccer Game Functions", () => {
    describe("getTotalPoints", () => {
        it("should return 10, 9 passed in to function", () => {
           
            const result = dealerShouldDraw([10,9])


            expect(result).toBe(false);
            

        })

        it("should return 'Ace', 6 passed in to function", () => {
           
            const result = dealerShouldDraw([10,9])


            expect(result).toBe(true);
            

        })

        it("should return 10, 6, 'Ace' passed in to function", () => {
           
            const result = dealerShouldDraw([10, 6, "Ace"])


            expect(result).toBe(false);
            

        })

        it("should return 2, 4 , 2, 5 passed", () => {
           
            const result = dealerShouldDraw([2, 4, 2, 5])


            expect(result).toBe(true);
            

        })

        
    })

})