// let myPromise = new Promise(function(resolve, reject) {
//   setTimeout(function() {
//     resolve();
//   }, 1000);
// });

// myPromise
//   .then(function() {
//     return 99;
//   })
//   .then(function(number) {
//     console.log(number);
//   });


const MyPromise =  new Promise(function (resolve, reject){
  setTimeout(function () {
    if(Math.random() >= 0.5){
      resolve();
    } else {
      reject();
    }
  }, 1000);
});

MyPromise
.then(() => {
  console.log("sucess");
})
.catch(() =>{
  console.log("fail");
})
.then(()=> {
  console.log("complete");
});