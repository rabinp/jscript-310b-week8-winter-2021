function change(callback, r) {
    r = r === undefined ? 0 : r;
    if (r >= 0 && r < 256) {
      const color = `rgb(${r}, ${r}, ${r})`;
      requestAnimationFrame(() => {
        console.log(color);  
        // debugger;
        document.body.style.backgroundColor = color;
        change(callback, r + 1);
      });
    } else {
      callback && callback();
    }
  }
  
  change(() => {
    alert('Transition complete');
  });
  