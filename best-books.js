let url="https://api.nytimes.com/svc/search/v2/articlesearch.json?q=tech&api-key=Uqf0GWELi1weGUr3L5TNHa9kYONPJbAU"
let headlines = document.getElementById("headlines")
const formEl = document.getElementById('best-books-form');
const yearEl = document.getElementById('year');
const monthEl = document.getElementById('month');
const dateEl = document.getElementById('date');
const booksContainer = document.getElementById("books-container");

formEl.addEventListener('submit', function(e) {
  e.preventDefault();

  const year = yearEl.value;
  const month = monthEl.value;
  const date = dateEl.value;

  const URL = `https://api.nytimes.com/svc/books/v3/lists/${year}-${month}-${date}/hardcover-fiction.json?api-key=${API_KEY}`

  // Fetch bestselling books for date and add top 5 to page

  fetch(URL)
  .then((result) => {
    return result.json();
  }).then((data) =>{
    const books = data.results.books;

    books.forEach((book) => {
      const newBook = document.createElement("div")
      newBook.classList = "book"
      const newBookHtml = `
                        <img src="${book.book_image}" />
                        <h3>${book.title}</h3>
                        <h5>${book.author}</h5>
                        <P>${book.description}</P>
                        `;
      newBook.innerHTML = newBookHtml
      booksContainer.appendChild(newBook);
      
    });

  });

  // fetch(url)
  // .then(response => response.json())
  // .then((date => {
  //   console.log(date);

  //   date.results.map(article=> {
  //     console.leg(article.title)
      
  //   })
  // }))
  


});
